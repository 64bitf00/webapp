package main

import (
	. "./app/routing"
	. "./db"
	"log"
	"net/http"
	"os"
)

type Environment struct {
	Name string
	Port string
	Host string
}

func configureEnvironment() Environment {
	env := os.Getenv("env")
	if env == "" {
		env = "local"
	}
	port := os.Getenv("port")
	if port == "" {
		port = "8080"
	}
	host := os.Getenv("host")
	if host == "" || env == "local" {
		host = "http://localhost"
	}

	return Environment{Name: env, Port: port, Host: host}
}

func main() {
	env := configureEnvironment()
	log.Printf("Application Environment: %s", env.Name)
	log.Printf("Application starting up: %s:%s", env.Host, env.Port)
	Setup(env.Name)
	http.HandleFunc("/create/", PageHandler(CreateHandler))
	http.HandleFunc("/edit/", PageHandler(EditHandler))
	http.HandleFunc("/save/", PageHandler(SaveHandler))
	http.HandleFunc("/delete/", PageHandler(DeleteHandler))
	http.HandleFunc("/resources/", ResourceHandler(RenderResource))
	http.HandleFunc("/", PageHandler(ViewHandler))
	if env.Name != "production" {
		err := RunMigrations()
		if err != nil {
			log.Fatal(err)
		}
	}
	log.Fatal(http.ListenAndServe(":"+env.Port, nil))
}
