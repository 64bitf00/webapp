# Welcome to the Repo!
This is designed and built to work on linux.
But may also work on other Operating Systems.

Developed on a PixelBook.

## Pre-requisites
* A working [Docker](https://docs.docker.com/install/) installation
* A working [Docker-Compose](https://docs.docker.com/compose/install/) installation

 > If your docker installation requires sudo, you'll probably need to change some stuff...
 > Or, just run as sudo - but I recommend adding your user to the docker group.
 `sudo usermod -a -G docker $USER`

## How to use
This is a self contained project.
once pulled, run:

`docker-compose up -d`

## Things to note:
This is primarily being built as a hobby project. 

As such, recommendations are always welcome, but likely all pull requests will be denied.
This is open-source because I feel it should be, and if it gains any significant following I'll ammend this policy.

## The Plan
Primarily, this is going to be base / foundation for a web framework in GO.

### Maintainers
* James King <jameshdking@gmail.com> 
