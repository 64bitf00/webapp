CREATE TABLE if not exists pages (
  page_id serial PRIMARY KEY,
  title TEXT,
  body TEXT,
  created_at TIMESTAMP,
  updated_at TIMESTAMP,
  slug TEXT
);